from unittest import TestCase
import yaml

from .DeepNetGuru import DNNGuru

def cfg_loader(path):
    return yaml.load(path, 'r').read())

def cfg_dump(path, cfg):
    yaml.dump(open(path, 'w'), cfg)

def cfg_remover(path):
    os.system('rm {}'.format(path))

class test_DeepNetGuru(TestCase):

	def test_initialize_guru(self):	
            cfg = cfg_loader('./cfg.yaml')
	    guru = DNNGuru(setname='test', cfg_path='./cfg.yaml')
            self.assertDictEqual(guru.cfg, cfg)

	def test_refresh_guru(self):
            guru = DNNGuru(setname='test', cfg_path='./cfg.yaml')
	    old_task = gury.cfg['task']
            setname="test2"
	    cfg = cfg_loader('./cfg.yaml')
            cfg_dumper('./cfg2.yaml', cfg)
            guru.refresh_set(setname,  cfg_path='./cfg2.yaml')
 	    self.assertEqual(guru.cfg['task'],old_task)
            self.assertEqual(guru.setname, 'test2')

	
	def test_get_advice_classify_text_binary(self) 
	    guru = DNNGuru(setname='test', cfg_path='./task_type/text/classify_images_binary/cfg.yaml')
	    map = guru.get_layers_map() 
	    model = guru.get_advice()


