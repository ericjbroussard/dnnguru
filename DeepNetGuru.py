
import keras
import os
import yaml
import time
import cPickle as pickle
import numpy as np
import scipy as sp
import scipy.stats as spat


from keras.models import Sequential
from keras.layers import Dense, Activation, Convolution2D, Dropout, MaxPooling2D, Flatten
from keras.optimizers import Adam, SGD, Adagrad
from keras.layers.normalization import BatchNormalization


#=========================================
#
#========================================
class IOProtege(object):

	def __init__(self):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

    	    self.cfg_default_path = "./cfg.yaml"
	    self.task_cfg_template="{task_type}/{task_id}/{cfg_name}.yaml"

	def fexists(self, path):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	    return os.path.exists(path)

	def load_cfg(self, path=None):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	    if path is None:
                path = self.cfg_default_path
	    if self.fexists(path):
	        return yaml.load(open(path, 'r').read())
	    else:
	        raise IOError, "config file does not exist"

	def save_cfg(self, cfg, path='./cfg.yaml'):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

            return yaml.dump(open(path, 'w'), cfg)
 
        def load_data_and_labels(self, path):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	    mat = np.load("{path}_mat.npy".format(path))
	    labels = np.load("{path}_lab.npy".format(path))
	    return mat, labels

	def save_data_and_labels(self, data, labels, path):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	    np.save("{}_mat.npy".format(path), data)
            np.save("{}_lab.npy".format(path), labels)



#===================================================
# 
#===================================================
class HyperParameterProtege(object):

	def __init__(self):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	    self.hpid = time.time()


	def get_convo_params(self, input_dims, border_mode, num_kerns,  kern_dims, stride=(1,1), dilation=(1,1)):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

            dilation_dims = (kern_dims[0]+ (kern_dims[0]-1)*(dilation[0]-1) ,
			     kern_dims[1]+ (kern_dims[1]-1)*(dilation[1]-1))
	    if border_mode == 'same':
	       return (num_kerns, (input_dims[0]+stride[0]-1)//stride[0], 
					  (input_dims[1]+stride[1]-1)//stride[1])
	    elif border_mode == "valid":
                return (num_kerns, ((input_dims[0]-dilation_dims[0]+1)+stride[0]-1)//stride[0],
				   ((input_dims[1]-dilation_dims[1]+1)+stride[1]-1)//stride[1])
	    elif border_mode == 'full':
                return (num_kerns, ((input_dims[0]+dilation_dims[0]-1)+stride[0]-1)//stride[0],
                                   ((input_dims[1]+dilation_dims[1]-1)+stride[1]-1)//stride[1])

	def get_rbm_params(self, input_dims, output_size):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

            return (self.input_dims, output_size)

        def get_mlp_params(self, input_dims, output_size):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

            return (self.input_dims, output_size)



class DataProtege(object):

	def __init__(self):
	    self.data =  None
 	    self.labels = None
	    self.train_data = None
	    self.train_labels = None
            self.valid_data = None
            self.valid_labels = None
	    self.num_folds = None

	def get_data(self):
	    return self.data

        def set_data(self, data):
	    self.data = data

        def get_labels(self):
            return self.labels

        def set_labels(self, labels):
            self.labels = labels

	def get_training_data(self):
            return self.train_data

	def get_training_labels(self):
            return self.train_labels

        def get_validation_data(self):
            return self.valid_data

        def get_validation_labels(self):
            return self.valid_labels

        def get_data_sets(self):
            return ((self.train_data, self.train_labels), (self.valid_data, self.valid_labels))

	def get_training_set(self):
            return (self.train_data, self.train_labels)

        def get_validation_set(self):
            return (self.valid_data, self.valid_labels)

	def normalize_data_by_const(self, normalizing_const):
	    self.data = self.data.astype(np.float32)/float(normalizing_const)

	def get_binary_examples_1type(self, bin_val,  data, labels):
	    bin_idxs = np.argwhere(labels==bin_val)
	    bin_data = []
	    pos_labs = []
	    for i in bin_idxs:
	       bin_data.append(data[i])
	       bin_labels.append(labels[i])
	    bin_data = np.array(bin_data)
	    bin_labels = np.array(bin_labels)
	    return pos_idxs, pos_data, pos_labels

	def balance_binary_samples(self):
	    pos_idxs, pos_data, pos_lab = self.get_binary_examples_1type(1, self.data, self.labels)
	    neg_idxs, neg_data, neg_lab = self.get_binary_examples_1type(0, self.data, self.labels)
            idxs_min = np.min(np.size(pos_idxs),np.size(neg_idxs)) 
	    balanced_data = []
	    balanced_labs = []
            for i in range(0, idxs_min):
                balanced_data.append([self.data[pos_idxs[i]]])
	        balanced_data.append([self.data[neg_idxs[i]]])
                balanced_labs.append([self.labels[pos_idx[i]]])
	        balanced_labs.append([self.labels[pos_idx[i]]])
	    self.data = np.array(balanced_data)
	    self.labels = np.array(balanced_labs)
	    #return balanced_data, balanced_labs

	def get_num_samples(self):
  	   return self.data.shape[0]

	def get_samples_per_fold(self, num_folds=5):
	   self.num_folds = num_folds
	   return int(np.ceil((self.get_num_samples())/float(self.num_folds)))

	def make_fold_sets(self, num_folds=5, training_folds=4, normalizing_const=255.0):
           set_size = self.get_samples_per_fold()
	   train_size = set_size*training_folds
	   valid_size = set_size*(num_folds - training_folds)
	   self.normalize_data_by_const(normalizing_const)
	   self.train_data, self.train_labs = self.data[:train_size], self.labels[:train_size]
	   self.valid_data, self.valid_labs = self.data[train_size:], self.labels[train_size:]


#==========================================
#
#==========================================
class DNNGuru(object):
	io_protege = IOProtege()
	hyper_protege = HyperParameterProtege()
	data_protege = DataProtege()

	def __init__(self, setname, cfg_path='./cfg.yaml'):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	    self.setname=setname
            self.cfg = self.io_guru.load_cfg(path=cfg_path)
	    self.input_shape = None
            self.output_shape = None
	    self.data = None
	    self.labels = None

	def set_input_dims(self, input_shape):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	    self.input_shape= input_shape
	    self.prev_dims = input_shape

        def set_output_dims(self, output_shape):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	    self.output_shape=output_shape

	def add_convolution_layer_same(self, model=None, **kwargs):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

            if model is None:
                model = Sequential(name="convo_valid")
            output_shape = self.hyper_protege.get_convo_params(self.prev_dims, 
                                                          'same',  self.cfg['num_kerns'], self.cfg['kern_dims'],
                                                          self.cfg['stride'], self.cfg['dilation'])

            if output_shape[0]*output_shape[1]*output_shape[2] > output_shape[-1]:
                    model.add(Convolution2D(self.cfg['num_kerns'], self.cfg['kern_dims'][0],
                                            self.cfg['kern_dims'][1], border_mode="valid",
                                            input_shape=self.prev_dims[1:]))
                    model.add(Activation(self.cfg["act_func"]))
                    model.add(Dropout(self.cfg['dropout_rate']))
                    self.prev_dims = (self.prev_dims[0], output_shape[0], output_shape[1], output_shape[2])
	    return model

	def add_convolution_layer_valid(self, model=None, **kwargs):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	    if model is None:
	  	model = Sequential(name="convo_valid")
	    output_shape = self.hyper_protege.get_convo_params(self.prev_dims, 
							  'valid',  self.cfg['num_kerns'], self.cfg['kern_dims'],
                                                          self.cfg['stride'], self.cfg['dilation'])
	    if output_shape[0]*output_shape[1]*output_shape[2] > output_shape[-1]:
		    model.add(Convolution2D(self.cfg['num_kerns'], self.cfg['kern_dims'][0], 
					    self.cfg['kern_dims'][1], border_mode="valid",
                                	    input_shape=self.prev_dims[1:]))
		    model.add(Activation(self.cfg["act_func"]))
	    	    model.add(MaxPooling2D(self.cfg['pool_dims']))
	    	    model.add(Dropout(self.cfg['dropout_rate']))
		    self.prev_dims = (self.prev_dims[0], output_shape[0], output_shape[1], output_shape[2])
	    return model

	def join_models(self, model_a, model_b=None, joint_type='flatten'):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	   if model_b is None:
              model_b = Sequential()
	   model_ab = Sequential()
	   if joint_type=='flatten':
              model_b.add(Flatten())
	      model_ab.add([model_a + model_b])
	      self.prev_dims = (self.prev_dims[0], reduce(lambda x,y: x*y, self.prev_dims[1:], 1))
	      return model_ab

	def add_rbm_layer(self, model=None, **kwargs):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	   if model is None:
    	       model = Sequential(name="rbm") 
           output_shape = self.hyper_protege.get_rbm_params(self.prev_dims, self.cfg['rbm_size'])
           model.add(Dense(output_shape[-1], 
			   input_shape=(self.prev_dims)))
	   model.add(Activation(self.cfg['act_func']))
           model.add(Dropout(self.cfg['dropout_rate']))
	   self.prev_dims = (self.prev_dims, output_shape)
	   return model

        def add_mlp_layer(self, model=None, **kwargs):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

           if model is None:
               model = Sequential(name="mlp")  

           output_shape = self.hyper_protege.get_mlp_params(self.prev_dims, self.cfg['mlp_size'])
           model.add(Dense(output_shape[-1], 
                           input_shape=(self.prev_dims)))
           model.add(Activation(self.cfg['act_func']))
           model.add(Dropout(self.cfg['dropout_rate']))
   	   self.prev_dims = (self.prev_dims, output_shape)
 	   return model

	def add_binary_classifier_layer(self, model=None):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	   if model is None:
               model = Sequential(name="binary_classifier")
	   output_shape = (self.prev_dims[0], 1)
           model.add(Dense(1, input_shape=self.prev_dims))
	   model.add(Activation(self.cfg['class_act_func']))
	   return model

	def add_multiclass_classifier_layer(self, model=None):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	   if model is None:
              model = Sequential(name="multiclass_classifier")
	   output_shape = (self.prev_dims[0], self.cfg['num_classes'])
           model.add(Dense(self.cfg['num_classes'],input_shape=self.prev_dims))
           model.add(Activation(self.cfg['class_act_func']))
           return model

	def classify_image_binary(self, model):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	   for i in range(0, self.cfg['num_convo_lays']):
	   	model = self.add_convolution_layer_valid(model=model)
	   model = self.join_models(model, model_b=Sequential(), joint_type='flatten')
 	   for j in range(0, self.cfg['num_rbm_lays']):
                model = self.add_rbm_layer(model=model)
           model = self.add_binary_classifier_layer(model)

	def classify_image_multiclass(self, model):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	   for i in range(0, self.cfg['num_convo_lays']):
                model = self.add_convolution_layer_valid(model=model)  
	   model = self.join_models(model, model_b=Sequential(), joint_type='flatten')
           for j in range(0, self.cfg['num_rbm_lays']):
                model = self.add_rbm_layer(model=model)
           model = self.add_multiclass_classifier_layer(model)


	def advise_model(self, model):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """
	   if self.cfg['task'] == 'classify_image_binary':
              model = self.classify_image_binary(model)
           elif self.cfg['task'] == 'classify_image_multiclass':
	      model = self.classifiy_image_multiclass(model)
	   elif self.cfg['task'] == 'classify_image_multilabel':
	      model = self.classify_image_multilabel(model)

	def get_optimizer(self, **kwargs):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	    if self.cfg['optimizer'] == 'adam':
               return Adam(**kwargs)
	    elif self.cfg['optimizer'] == 'sgd':
	       return SGD(**kwargs)

	def multiclass_optimizers_params(self, opt_params=None):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	   if opt_params is None:
		opt_params = dict()
	   if self.cfg['optimizer'] == 'adam':
		return opt_params.update( dict(learning_rate=self.cfg['learning_rate'],
					    momentum=self.cfg['momentum']))
 	   elif self.cfg['optimizer'] == 'sgd':
		return opt_params.update(dict(learning_rate=self.cfg['learning_rate']))

	def advise_optimizer(self, opt_params=None):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	  if opt_params is None:
             opt_params = dict()
	  if self.cfg['task'] == 'classify_image_multiclass':
		opt_params = self.multiclass_optimizers_params(opt_params=opt_params)
 		opt = self.get_optimizer(**opt_params)
		return opt

	def advise_loss_func(self):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	    if self.cfg['task'] == 'class_image_multclass':
	        if self.cfg['class_act_func'] == 'sigmoid':
		    return 'mse'
		elif self.cfg['class_act_func'] == 'relu':
                    return 'binary_crossentropy'
                elif self.cfg['class_act_func'] == 'softmax':
		    return 'categorical_crossentropy'

	def advise_metrics(self):
            """ 
            PreCond:
            PostCond:
            Input:
            Output:
            """

	    mets = ['accuracy']
	    if self.cfg['task'] == 'class_image_multclass':
                if self.cfg['loss_func'] == 'mse':
                    mets.append('mse')
                elif self.cfg['loss_func'] == 'categorical_crossentropy':
                    mets.append('mse')
                elif self.cfg['loss_func'] == 'binary_crossentropy':
                    mets.append('binary_accuracy')
	    return mets

	def get_advice(self):
	    """
	    PreCond:
	    PostCond:
	    Input:
	    Output:
	    """
            model = Sequential(name="GuruAdvice")
            model = self.advise_model(model)
	    self.cfg['opt'] = self.advise_optimizer()
	    self.cfg['loss_func'] = self.advise_loss_func()
	    self.cfg['metrics'] = self.advise_metrics()
	    return model

	def train_model(self, model=None):
	    data, lab = self.io_protege.get_data_and_labels("{}/{}/{}".format(self.cfg['task'], self.setname, self.setname))
	    data_protege.set_data(data)
	    data_protege.set_labels(lab)
	    data_protege.balance_binary_samples().make_fold_sets()
	    train_set, valid_set = data_protege.get_data_sets()
	    x_train, y_train = train_set
            x_valid, y_valid = valid_set
	    model = self.get_advice()
	    model.compile(loss=self.cfg['loss_func'],
                          optimizer=opt,
                          metrics=self.cfg['metrics'])
	    model.fit([x_train, y_train]))

	
